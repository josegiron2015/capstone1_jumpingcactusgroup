﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguePauseMenu : MonoBehaviour {

	public static bool IsGamePaused = false;

	// Update is called once per frame
	void Update () 
	{
		
			
	}

	public static void Resume()
	{
		
		Time.timeScale = 1f;
		IsGamePaused = false;
		Debug.Log ("RESUME VOID WORKING");
	}

	public static void Paused()
	{
		
		Time.timeScale = 0f;
		IsGamePaused = true;
		Debug.Log ("PAUSE VOID WORKING");
	}
}
