﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllerScenes : MonoBehaviour {

    public GameObject mainCamera;
    public GameObject CutsceneCamera1;


    public void CutsceneCamera1On()
    {
        mainCamera.SetActive(false);
        CutsceneCamera1.SetActive(true);
    }

    public void CutsceneCamera1Off()
    {
        mainCamera.SetActive(true);
        CutsceneCamera1.SetActive(false);
    }
}
