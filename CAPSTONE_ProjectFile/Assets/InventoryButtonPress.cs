﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class InventoryButtonPress : MonoBehaviour {
    public PlayerScript player;
    public bool isOpen = false;
    public EventSystem eventSystem;
    public Button CloseButton;
    public Button myButton;
    public Animator animator;
	
    public void OpenInventory()
    {
		eventSystem.sendNavigationEvents = true;
		myButton.Select();
		isOpen = true;
        animator.SetBool("isOpen", true);
        player.CanMove = true;
    }

    public void CloseInventory()
    {
        animator.SetBool("isOpen", false);
        isOpen = false;
		CloseButton.OnDeselect (null);
		eventSystem.SetSelectedGameObject(null);
		eventSystem.sendNavigationEvents = false;
        player.CanMove = false;
    }

    // Update is called once per frame
    void Update ()
    {
		if ( Input.GetButtonDown("PadPress") && !isOpen ||Input.GetKeyDown(KeyCode.I) && !isOpen)
        {
            isOpen = true;
            Debug.Log("Inventory is open.");
            OpenInventory();           
        }

        else if (Input.GetButtonDown("PadPress") && isOpen || Input.GetKeyDown(KeyCode.I) && isOpen)
        {
            isOpen = false;
            Debug.Log("Inventory is closed.");
            CloseInventory();
        }
        //    // Select the button
        //    Debug.Log("Button pressed");
        //EventSystem.current.SetSelectedGameObject(myButton.gameObject);
        //// Highlight the button
        //myButton.OnSelect(new BaseEventData(EventSystem.current));

    }
}
