﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyDetectionScript : MonoBehaviour {

	public Transform nearByEnemy;

	void OnTriggerStay(Collider other)
	{
		if(other.gameObject.tag == "Sounds")
		{
			nearByEnemy = other.gameObject.transform;
			this.GetComponentInParent<EnemyFSM>().SoundToInvestigate = nearByEnemy;
		}
    }
}
