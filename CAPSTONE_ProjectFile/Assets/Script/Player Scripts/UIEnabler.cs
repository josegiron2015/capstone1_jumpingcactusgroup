﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEnabler : MonoBehaviour
{
    public GameObject target;
    public FSM fsmScript;
    
	// Use this for initialization
	void Start ()
    {
        fsmScript = GetComponent<FSM>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.E)) 
        {
          
            target.SetActive(!target.activeSelf);
            //fsmScript.CursorLock = false;
            if (target.activeSelf)
            {
                Debug.Log("Inventory Enabled");
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else if (!target.activeSelf)
            {
                Debug.Log("Inventory Disabled");
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
                
        }
    }

    
}
